﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns="urn:schemas-cybersource-com:transaction-data-1.41"
	xmlns:sec="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">

	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="requestMessage">
		<s:Envelope>
			<s:Header>
				<sec:Security mustUnderstand="1">
					<sec:UsernameToken Id="uuid-90128b0b-6212-4e16-9382-e55489fa6444-1">
						<sec:Username><xsl:value-of select="merchantID"/></sec:Username>
						<sec:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wssusername-token-profile-1.0#PasswordText">neXmpqESclCYS2X4ZYg5TzeXOt/PPI0jl97YQwsoi3QTD1wg77mYl95lTf5uxhoFMyReuiliSRfxtzwK4R8oE+bRkNcwggJ2AgEA5EGbDxMW2BOHpUK8WAUhSxlY1ZkpRArzFi6YpVpOjkG0N0q6q2rzVljWjfKuNxVkya3IdE1sojLDjqPF7TC3v5JliDlPN5c63888jSOX3thDCyiLdBMPXCDvuZiX3mVN/m7GGgUzJF66KWJJF/G3PArhHygT5tGQ1zCCAnYCAQDkQZsPExbYE4elQrxYBSFLGVjVmSlECvMWLpilWk6OQbQ3SrqravNWWNaN8q43FWTJrch0TQ==</sec:Password>
					</sec:UsernameToken>
				</sec:Security>
			</s:Header>
			<s:Body>
				<requestMessage>
					<xsl:apply-templates select="*"/>
				</requestMessage>
			</s:Body>
		</s:Envelope>
	</xsl:template>

	<!-- The purpose of this is to not get the namespace -->
	<xsl:template match="*">
		<xsl:element name="{local-name()}">
			<xsl:apply-templates select="@* | node()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="@*">
		<xsl:copy-of select="." />
	</xsl:template>


</xsl:stylesheet>
