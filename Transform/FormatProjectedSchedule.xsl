﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" />
	<xsl:output omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:key name="installment-date" match="Installment" use="InstallmentDate" />
	<xsl:key name="due-date" match="Installment" use="DueDate" />

	<xsl:template match="/">
		<InstallmentSchedule Type="Current">
			<xsl:for-each select="//ProjectedScheduleData/Receivable/Installments/Installment[count(. | key('installment-date', InstallmentDate)[1]) = 1]">
				<xsl:sort select="InstallmentDate" />
				<xsl:call-template name="InstallmentDate"/>
			</xsl:for-each>
		</InstallmentSchedule>

	</xsl:template>

	<xsl:template name="InstallmentDate">

		<xsl:variable name="installmentDate" select="InstallmentDate"/>
		<xsl:variable name="receivableType" select="ancestor::Receivable/@Type" />
		<InstallmentDate InstallmentDate="{InstallmentDate}">


			<xsl:attribute name="DueDate">
				<xsl:value-of select="DueDate" />
			</xsl:attribute>
			<xsl:attribute name="InstallmentAmount">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/InstallmentAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="ClosedToCash">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/ItemClosedToCashAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="ClosedToCredit">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/ItemClosedToCreditAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="ClosedWriteOff">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/ItemClosedToWriteOffAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="ClosedRedistributed">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/ItemRedistributedAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="OriginalDueDate">
				<xsl:value-of select="ItemOriginalDueDate" />
			</xsl:attribute>
			<xsl:attribute name="Type">
				<xsl:value-of select="Type" />
			</xsl:attribute>
			<xsl:call-template name="SubGroup" />
		</InstallmentDate>
	</xsl:template>

	<xsl:template name="SubGroup">
		<xsl:variable name="installmentDate" select="InstallmentDate"/>
		<xsl:variable name="receivableType" select="ancestor::Receivable/@Type" />
		<SubGroup InstallmentDate="{InstallmentDate}">

			<xsl:attribute name="ScheduleSubGroupDescription">
				<xsl:value-of select="ancestor::Receivable/@PolicyReference" />
			</xsl:attribute>
			<xsl:attribute name="DueDate">
				<xsl:value-of select="DueDate" />
			</xsl:attribute>
			<xsl:attribute name="InstallmentAmount">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/InstallmentAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="ClosedToCash">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/ItemClosedToCashAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="ClosedToCredit">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/ItemClosedToCreditAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="ClosedWriteOff">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/ItemClosedToWriteOffAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="ClosedRedistributed">
				<xsl:value-of select ="sum(//Installment[InstallmentDate=$installmentDate]/ItemRedistributedAmount)"/>
			</xsl:attribute>
			<xsl:attribute name="OriginalDueDate">
				<xsl:value-of select="ItemOriginalDueDate" />
			</xsl:attribute>
			<xsl:attribute name="Type">
				<xsl:value-of select="Type" />
			</xsl:attribute>

			<xsl:for-each select="key('due-date', DueDate)">
				<xsl:sort select="ancestor::Receivable/@AllocationPriority" />
				<xsl:call-template name="Installment" />
			</xsl:for-each>
		</SubGroup>

	</xsl:template>

	<xsl:template name="Installment">
<xsl:variable name="receivableType" select="ancestor::Receivable/@Type" />
		<Installment DueDate="{DueDate}">
			<xsl:attribute name="DetailTypeCode">
				<xsl:choose>
					<xsl:when test="not($receivableType='CH')">
						<xsl:value-of select="'IS'"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="'CH'"/>
					</xsl:otherwise>
				</xsl:choose>
				
			</xsl:attribute>
			<xsl:attribute name="TransactionDate">
				<xsl:value-of select="ancestor::Receivable/@TransactionDate" />
			</xsl:attribute>
			<xsl:attribute name="ItemEffectiveDate">
				<xsl:value-of select="ancestor::Receivable/@ItemEffectiveDate" />
			</xsl:attribute>

			<xsl:attribute name="ItemAmount">
				<xsl:value-of select="ancestor::Receivable/@ItemAmount" />
			</xsl:attribute>
			<xsl:attribute name="BillItemId">
				<xsl:value-of select="ancestor::Receivable/@BillItemId" />
			</xsl:attribute>
			<xsl:attribute name="PolicyTermId">
				<xsl:value-of select="ancestor::Receivable/@PolicyTermId" />
			</xsl:attribute>

			<xsl:attribute name="DetailId">
				<xsl:value-of select="@id"/>
			</xsl:attribute>
			<xsl:attribute name="InstallmentTypeCode">
				<xsl:value-of select="InstallmentTypeCode"/>
			</xsl:attribute>
			<xsl:attribute name="CoverageReference">
				<xsl:value-of select="ancestor::Receivable/@CoverageReference" />
			</xsl:attribute>

			<xsl:attribute name="ReceivableTypeCode">
				<xsl:value-of select="ancestor::Receivable/@Type" />
			</xsl:attribute>
			<xsl:attribute name="TransactionTypeCode">
				<xsl:value-of select="ancestor::Receivable/@TransactionTypeCode" />
			</xsl:attribute>
			<xsl:attribute name="InstallmentAmount">
				<xsl:value-of select="InstallmentAmount" />
			</xsl:attribute>
			<xsl:attribute name="ClosedToCash">
				<xsl:value-of select="ItemClosedToCashAmount" />
			</xsl:attribute>
			<xsl:attribute name="ClosedToCredit">
				<xsl:value-of select="ItemClosedToCreditAmount" />
			</xsl:attribute>
			<xsl:attribute name="ClosedWriteOff">
				<xsl:value-of select="ItemClosedToWriteOffAmount" />
			</xsl:attribute>
			<xsl:attribute name="ClosedRedistributed">
				<xsl:value-of select="ItemRedistributedAmount" />
			</xsl:attribute>
		</Installment>
	</xsl:template>
</xsl:stylesheet>