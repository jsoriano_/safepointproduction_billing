﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:key name="quoteDueDateKey" match="//Schedules/Schedule/Installments/Installment/DueDate/text()" use="." />

	<xsl:template match="/">
		<xsl:call-template name="BuildInstallmentSchedule"/>
	</xsl:template>

	<xsl:template name="BuildInstallmentSchedule">
		<Installments>
			<xsl:choose>
				<xsl:when test="//Schedules/Schedule/Installments/Installment/InstallmentAmount">
					<Based>quote</Based>
					<xsl:for-each select="//Schedules/Schedule/Installments/Installment/DueDate/text()[generate-id() = generate-id(key('quoteDueDateKey',.)[1])]">
						<xsl:call-template name="BuildQuoteBasedInstallments"/>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<Based>committed</Based>
					<xsl:for-each select="//InstallmentSchedule/InstallmentDate">
						<xsl:call-template name="BuildCommittedBasedInstallments"/>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</Installments>
	</xsl:template>
	
	<xsl:template name="BuildQuoteBasedInstallments">
		<xsl:variable name="dueDate" select="."/>
		<xsl:if test="//Schedules/Schedule/Installments/Installment[DueDate=$dueDate and ItemClosedIndicator!='Y']">
			<Installment>
				<DueDate>
					<xsl:value-of select="$dueDate"/>
				</DueDate>
				<xsl:for-each select="//Schedules/Schedule/Installments/Installment[DueDate=$dueDate]">
					<xsl:call-template name="BuildQuoteSubInstallments"/>
				</xsl:for-each>
				<xsl:call-template name="BuildQuoteBasedCommittedInstallments">
					<xsl:with-param name="dueDate">
						<xsl:value-of select="$dueDate"/>
					</xsl:with-param>
				</xsl:call-template>
			</Installment>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="BuildQuoteSubInstallments">
		<QuoteSubInstallment>
			<SubInstallmentAmount>
				<xsl:value-of select="InstallmentAmount"/>
			</SubInstallmentAmount>
		</QuoteSubInstallment>
	</xsl:template>

	<xsl:template name="BuildQuoteBasedCommittedInstallments">
		<xsl:param name="dueDate"/>
		<xsl:for-each select="//InstallmentSchedule/InstallmentDate/SubGroup">
			<xsl:if test="../SubGroup/Installment[@DueDate = $dueDate]">
				<xsl:call-template name="BuildCommittedSubGroups"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="BuildCommittedBasedInstallments">
		<Installment>
			<DueDate>
				<xsl:value-of select="SubGroup/Installment/@DueDate"/>
			</DueDate>
			<xsl:for-each select="SubGroup">
				<xsl:call-template name="BuildCommittedSubGroups"/>
			</xsl:for-each>
		</Installment>
	</xsl:template>

	<xsl:template name="BuildCommittedSubGroups">
		<CommittedSubGroup>
			<SubGroupAmount>
				<xsl:value-of select="@InstallmentAmount"/>
			</SubGroupAmount>
			<xsl:for-each select="Installment">
				<xsl:call-template name="BuildCommittedSubInstallments"/>
			</xsl:for-each>
		</CommittedSubGroup>
	</xsl:template>

	<xsl:template name="BuildCommittedSubInstallments">
		<CommittedSubInstallment>
			<SubInstallmentAmount>
				<xsl:value-of select="@InstallmentAmount"/>
			</SubInstallmentAmount>
			<SubInstallmentClosedToCreditAmount>
				<xsl:value-of select="@ClosedToCredit"/>
			</SubInstallmentClosedToCreditAmount>
			<SubInstallmentClosedWriteOffAmount>
				<xsl:value-of select="@ClosedWriteOff"/>
			</SubInstallmentClosedWriteOffAmount>
			<SubInstallmentClosedRedistributedAmount>
				<xsl:value-of select="@ClosedRedistributed"/>
			</SubInstallmentClosedRedistributedAmount>
			<SubInstallmentTransactionTypeCode>
				<xsl:value-of select="@TransactionTypeCode"/>
			</SubInstallmentTransactionTypeCode>
			<SubInstallmentTransactionDate>
				<xsl:value-of select="@TransactionDate"/>
			</SubInstallmentTransactionDate>
		</CommittedSubInstallment>
	</xsl:template>

</xsl:stylesheet>
