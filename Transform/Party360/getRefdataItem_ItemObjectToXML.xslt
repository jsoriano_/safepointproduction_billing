﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:d4p1="http://claims.accenture.com/2011/01/01/Types" xmlns:datacontract="http://schemas.datacontract.org/2004/07/AFS.AEF"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:Util="urn:XsltUtility"
                exclude-result-prefixes="xs msxsl xsl d4p1 datacontract Util" >
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="ReferenceData/RefDataItems">
    <RefDataItems>
      <xsl:for-each select="Item">
        <xsl:call-template name="RefDataItemTemplate" />
      </xsl:for-each>
      <xsl:call-template name="ResponseInfo">
        <xsl:with-param name="ReturnCode">0</xsl:with-param>
        <xsl:with-param name="NotificationResponse"></xsl:with-param>
      </xsl:call-template>
    </RefDataItems>
  </xsl:template>

  <xsl:template name="RefDataItemTemplate">
    <RefDataItem>
      <Key>
        <xsl:value-of select="m_key"/>
      </Key>
      <LocaleId>
        <xsl:value-of select="m_localeId"/>
      </LocaleId>
      <Value>
        <xsl:value-of select="m_value"/>
      </Value>
      <Status>
        <xsl:value-of select="m_status"/>
      </Status>
      <SortOrder>
        <xsl:value-of select="m_sortOrder"/>
      </SortOrder>
      <BeginDate>
        <xsl:value-of select="string(Util:ConvertDateToDuckFormat(m_beginDate))"/>
      </BeginDate>
      <EndDate>
        <xsl:value-of select="string(Util:ConvertDateToDuckFormat(m_endDate))"/>
      </EndDate>
      <MappedCategory>
        <xsl:value-of select="m_mappedCategory"/>
      </MappedCategory>
      <MappedKey>
        <xsl:value-of select="m_mappedKey"/>
      </MappedKey>
      <ExtendedFields>
        <xsl:call-template name="ExtendedFieldTemplate" />
      </ExtendedFields>

    </RefDataItem>
  </xsl:template>

  <xsl:template name="ExtendedFieldTemplate">
    <xsl:for-each select="m_extendedFields/KeyValueOfstringstring">
      <ExtendedField>
        <Name>
          <xsl:value-of select="Key"/>
        </Name>
        <Value>
          <xsl:value-of select="Value"/>
        </Value>
      </ExtendedField>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="ResponseInfo">
    <xsl:param name="ReturnCode">
    </xsl:param>
    <xsl:param name="NotificationResponse"></xsl:param>
    <ResponseInfo>
      <ReturnCode>
        <xsl:value-of select="$ReturnCode"/>
      </ReturnCode>
      <xsl:choose>
        <xsl:when test="$NotificationResponse != ''">
          <NotificationResponse>
            <xsl:value-of select="$NotificationResponse"/>
          </NotificationResponse>
        </xsl:when>
        <xsl:otherwise>
          <NotificationResponse />
        </xsl:otherwise>
      </xsl:choose>
    </ResponseInfo>
  </xsl:template>

</xsl:stylesheet>
