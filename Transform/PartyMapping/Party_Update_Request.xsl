﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:template match="PartyRecord">
		<Request.ifRq condition="true">
			<Session.setDocumentRq document="_PartyData">
				<_PartyData>
					<PartyRecord>
					</PartyRecord>
				</_PartyData>
			</Session.setDocumentRq>
			<PartyObj.getPartyForUpdateRq>
				<PartyId><xsl:value-of select="@partyId"/></PartyId>
				<TransactionDate><xsl:value-of select="@transactionDate"/></TransactionDate>
				<PartyRecordXPath>_PartyData/PartyRecord</PartyRecordXPath>
			</PartyObj.getPartyForUpdateRq>
			<xsl:for-each select="Party/*">
				<Session.removeElementRq path="_PartyData^PartyRecord/Party/{local-name()}" />
				<Session.setLeafRq path="_PartyData^PartyRecord/Party">
					<xsl:copy-of select="."/>
				</Session.setLeafRq>
			</xsl:for-each>
			<PartyObj.updatePartyByXPathRq TransactionDate="{@transactionDate}" ReturnXPath="_PartyData" RemoveXPath="_PartyData/PartyRecord" RequestXPath="_PartyData/PartyRecord" RequestReturnData="N" />
		</Request.ifRq>
	</xsl:template>
</xsl:stylesheet>
