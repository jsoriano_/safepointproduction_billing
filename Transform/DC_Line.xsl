﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<!-- Each line is going to setup a template match based on type and indicator -->
	<xsl:template match="line"/>
	<xsl:template name="dc:Line.Body">
		<DC_Line Id="{@id}">
			<Type><xsl:value-of select="Type"/></Type>
			<HonorRates><xsl:value-of select="HonorRates"/></HonorRates>
			<PolicyType><xsl:value-of select="exposure[Type='PolicyType']/sValue"/></PolicyType>
			<HonoredRateEffectiveDate><xsl:value-of select="HonoredLineRateEffectiveDate"/></HonoredRateEffectiveDate>
			<AssignmentDate><xsl:value-of select="exposure[Type='AssignmentDate']/sValue"/></AssignmentDate>
			<AuditPeriod><xsl:value-of select="exposure[Type='AuditPeriod']/sValue"/></AuditPeriod>
			<xsl:call-template name="dc:Line.Extensions"/>
			<xsl:call-template name="dc:Line.Relationships"/>
		</DC_Line>
	</xsl:template>
	<xsl:template name="dc:Line.Extensions"/>
	<xsl:template name="dc:Line.Relationships"/>
</xsl:stylesheet>

