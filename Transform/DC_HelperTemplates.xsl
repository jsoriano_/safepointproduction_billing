﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<xsl:template name="dc:Helper.GetTypeData">
		<xsl:choose>
			<xsl:when test="sValue">
				<Value><xsl:value-of select="sValue"/></Value>
				<DataType>nv</DataType>
			</xsl:when>
			<xsl:when test="iValue">
				<Value><xsl:value-of select="iValue"/></Value>
				<DataType>in</DataType>
			</xsl:when>
			<xsl:when test="bValue">
				<Value><xsl:value-of select="bValue"/></Value>
				<DataType>bt</DataType>
			</xsl:when>
			<xsl:when test="fValue">
				<Value><xsl:value-of select="fValue"/></Value>
				<DataType>fl</DataType>
			</xsl:when>
			<xsl:when test="dValue">
				<Value><xsl:value-of select="dValue"/></Value>
				<DataType>dt</DataType>
			</xsl:when>
			<xsl:when test="Description">
				<Value><xsl:value-of select="Description"/></Value>
				<DataType>nv</DataType>
			</xsl:when>
			<xsl:when test="LongDescription">
				<Value><xsl:value-of select="LongDescription"/></Value>
				<DataType>nv</DataType>
			</xsl:when>
			<xsl:when test="FormsDescription">
				<Value><xsl:value-of select="FormsDescription"/></Value>
				<DataType>nv</DataType>
			</xsl:when>
			<xsl:when test="FormsLongDescription">
				<Value><xsl:value-of select="FormsLongDescription"/></Value>
				<DataType>nv</DataType>
			</xsl:when>
			<xsl:otherwise>
				<DataType>nv</DataType>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
