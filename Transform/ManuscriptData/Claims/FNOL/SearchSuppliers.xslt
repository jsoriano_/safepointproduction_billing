﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslc="http://claims.accenture.com/2011/01/01/Types" xmlns:d2p1="http://schemas.datacontract.org/2004/07/AFS.Claims.Desktop.Types.SupplierManagement" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl xslc d2p1">
	<xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<Suppliers>
			<xsl:if test="count(FNOL.searchFNOLSuppliersRs/SearchSuppliersResponse/SupplierSearchResults/SupplierSearchResultsDTO) > 0">
				<xsl:for-each select="FNOL.searchFNOLSuppliersRs/SearchSuppliersResponse/SupplierSearchResults/SupplierSearchResultsDTO">
					<SupplierDetails>
						<FullAddress>
							<xsl:value-of select="Supplier/SupplierPartyDetail/PartyBusinessDetail/PartyAddressDetail/ClaimsPartyAddressDetailDTO/Address/FullAddress"/>
						</FullAddress>
						<Latitude>
							<xsl:value-of select="Supplier/SupplierPartyDetail/PartyBusinessDetail/PartyAddressDetail/ClaimsPartyAddressDetailDTO/Address/Latitude"/>
						</Latitude>
						<Longitude>
							<xsl:value-of select="Supplier/SupplierPartyDetail/PartyBusinessDetail/PartyAddressDetail/ClaimsPartyAddressDetailDTO/Address/Longitude"/>
						</Longitude>
						<DistanceFromAddress>
							<xsl:value-of select="DistanceFromAddress"/>
						</DistanceFromAddress>
						<FullName>
							<xsl:value-of select="Supplier/SupplierPartyDetail/PartyBusinessDetail/PartyBusNameDetail/ClaimsPartyBusNameDetailDTO/PartyBusName/Name"/>
						</FullName>
						<ContactNumber>
							<xsl:value-of select="Supplier/SupplierPartyDetail/PartyBusinessDetail/PartyPhone/ClaimsPartyPhoneDTO/FullPhoneNumber"/>
						</ContactNumber>		
						<Speciality>
						    <xsl:value-of select="Supplier/SupplierServiceDetails/SupplierServiceDetailDTO[1]/SupplierServiceSpecialities/SupplierServiceSpecialityDTO/Speciality"/>
						</Speciality>
						<Rating>
							<xsl:value-of select="Supplier/SupplierServiceDetails/SupplierServiceDetailDTO[1]/SupplierServices/Rating"/>
						</Rating>
						<SupplierServiceID>
						 <xsl:value-of select="Supplier/SupplierServiceDetails/SupplierServiceDetailDTO[1]/SupplierServices/SupplierServiceID"/>
						</SupplierServiceID>
					</SupplierDetails>
				</xsl:for-each>
			</xsl:if>
		</Suppliers>
	</xsl:template>
</xsl:stylesheet>