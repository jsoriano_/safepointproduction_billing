﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:template match="PartyRecord">
		<PartyRecord>
			<xsl:copy-of select="PartyRecordPartyId"/>
			<xsl:apply-templates select="Party | PartyPhones/PartyPhone | PartyEmails/PartyEmail | Locations/Location | PartyLicenses/PartyLicense"/>
		</PartyRecord>
	</xsl:template>

	<xsl:template match="Party | PartyPhone | PartyEmail | Location | PartyLicense">
		<xsl:copy-of select="."/>
	</xsl:template>

</xsl:stylesheet>

