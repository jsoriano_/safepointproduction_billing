﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="BillingTransactionCommon.xsl"/>
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:apply-templates></xsl:apply-templates>
	</xsl:template>

	<!-- suppress default with this template -->
	<xsl:template match="text()"/>

	<xsl:template match="Session.getAllDocumentsRs/_BillingInputDoc/BillingKeys | root/_BillingInputDoc/BillingKeys">
		<!-- The template match sets the correct context node. If the oneshot is called locally, the root will be <root>
	     If the oneshot is called remotely the root will be <Session.getAllDocumentsRs> -->
		<xsl:variable name="activeTransactionId" select="(/*/_processingData/activeTransactionID)" />
		<xsl:variable name="activeTransactionTypeCode" select="(/*/_processingData/transactions/transaction[@id=$activeTransactionId]/Type)" />
		<xsl:variable name="transactionTypeCode">
			<xsl:call-template name="LookupTransactionTypeCode">
				<xsl:with-param name="code">
					<xsl:value-of select="$activeTransactionTypeCode"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		 
		<OneShotBillingSetUp status="success">
			<!--this attribute is here to give the manuscript something to look for to check for success. DO NOT REMOVE IT -->
			<xsl:call-template name="AccountSetup"></xsl:call-template>

			<xsl:call-template name="PolicyTermSetup">
				<xsl:with-param name="transactionTypeCode" select="$transactionTypeCode"/>
			</xsl:call-template>
			
			<xsl:call-template name="DownPayment"></xsl:call-template>
			
		</OneShotBillingSetUp>
	</xsl:template>
	<xsl:template name="AccountSetup">
		<xsl:choose>
			<xsl:when test="normalize-space(AttachToAccountId) != ''">
				<!--Accountid or Account Reference -->
				<PartySetup>
					<LookupAgent name="AgentParty">
						<AgencyId>
							<xsl:value-of select="AgencyId"></xsl:value-of>
						</AgencyId>
					</LookupAgent>
					<LookupRole name="InsuredParty">
						<RoleInPolicy>I</RoleInPolicy>
					</LookupRole>
				</PartySetup>
				<AccountSetup>
					<Use>
						<xsl:attribute name="AccountId">
							<xsl:value-of select="AttachToAccountId"></xsl:value-of>
						</xsl:attribute>
					</Use>
				</AccountSetup>
			</xsl:when>
			<xsl:when test="normalize-space(AttachToAccountReference) != ''">
				<PartySetup>
					<LookupAgent name="AgentParty">
						<AgencyId>
							<xsl:value-of select="AgencyId"></xsl:value-of>
						</AgencyId>
					</LookupAgent>
					<LookupRole name="InsuredParty">
						<RoleInPolicy>I</RoleInPolicy>
					</LookupRole>
				</PartySetup>
				<AccountSetup>
					<Lookup>
						<AccountReference>
							<xsl:value-of select="AttachToAccountReference"></xsl:value-of>
						</AccountReference>
					</Lookup>
				</AccountSetup>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="NewAccountSetup">
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="NewAccountSetup">
		<PartySetup>
			<LookupAgent name="AgentParty">
				<AgencyId>
					<xsl:value-of select="AgencyId"></xsl:value-of>
				</AgencyId>
			</LookupAgent>
			<LookupRole name="InsuredParty">
				<RoleInPolicy>I</RoleInPolicy>
			</LookupRole>
			<Use name="PayorParty" party="InsuredParty"/>
			<Use name="PayeeParty" party="InsuredParty"/>
		</PartySetup>
		<AccountSetup>
			<Add>
				<LookupConfig>
					<AccountPlanCode>
						<xsl:value-of select="AccountPlanCode"></xsl:value-of>
					</AccountPlanCode>
					<!-- BillClass is included here for backward compatibility. New Plan manuscripts will use Account Plan Code. -->
					<BillClass>
						<xsl:value-of select="BillClass"></xsl:value-of>
					</BillClass>
					<BillType>
						<xsl:value-of select="BillType"></xsl:value-of>
					</BillType>
					<CollectionMethod>
						<xsl:value-of select="CollectionMethod"/>
					</CollectionMethod>
					<!-- Target due day logic is typically in the account plan manuscript and would be based on the policy effective date 
						 The value can be overridden here if necessary -->
					<TargetDueDays>
						<xsl:choose>
							<xsl:when test="normalize-space(TargetDueDay) = ''"></xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="TargetDueDay"></xsl:value-of>
							</xsl:otherwise>
						</xsl:choose>
					</TargetDueDays>
					<ElectronicInvoice>
						<xsl:value-of select="ElectronicInvoice"></xsl:value-of>
					</ElectronicInvoice>
				</LookupConfig>
				<AccountRecord>
					<ReportingGroupCode>
						<xsl:value-of select="AccountProcessingOrgUnitCode"></xsl:value-of>
					</ReportingGroupCode>
					<xsl:choose>
						<!-- This section left for backward compatibility with Billing Base Product. It can be removed once we switche to the new sample product -->
						<xsl:when test="EFTAccountNumber != ''">
							<EFTPaymentDetails>
								<BankAccountNumber>
									<xsl:value-of select="EFTAccountNumber"></xsl:value-of>
								</BankAccountNumber>
								<AccountType>
									<xsl:value-of select="EFTAccountType"></xsl:value-of>
								</AccountType>
								<LastName>
									<xsl:value-of select="EFTAccountLastName"></xsl:value-of>
								</LastName>
								<FirstName>
									<xsl:value-of select="EFTAccountFirstName"></xsl:value-of>
								</FirstName>
								<BankRoutingNumber>
									<xsl:value-of select="EFTRoutingNumber"></xsl:value-of>
								</BankRoutingNumber>
								<BankName>
									<xsl:value-of select="EFTBankName"></xsl:value-of>
								</BankName>
								<Activated>
									<xsl:choose>
										<xsl:when test="EFTActivated != ''">
											<xsl:value-of select="EFTActivated"></xsl:value-of>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>0</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</Activated>
							</EFTPaymentDetails>
						</xsl:when>
						<!-- This section populates the Auto Pay Bank details. EFT Payment Details would be populated if the collection method is Auto Pay Bank -->
						<xsl:when test="CollectionMethod = 'EFT'">
						  <EFTPaymentDetails>
							<xsl:attribute name="id">
							  <xsl:value-of select="@id"/>
							</xsl:attribute>
							<AccountName>
							  <xsl:value-of select="EFTPaymentDetails/AccountName"></xsl:value-of>
							</AccountName>
							<Activated>
							  <xsl:value-of select="EFTPaymentDetails/Activated"></xsl:value-of>
							</Activated>
							<LastName>
							  <xsl:value-of select="EFTPaymentDetails/LastName"></xsl:value-of>
							</LastName>
							<BankRoutingNumber>
							  <xsl:value-of select="EFTPaymentDetails/BankRoutingNumber"></xsl:value-of>
							</BankRoutingNumber>
							<BankAccountNumber>
							  <xsl:value-of select="EFTPaymentDetails/BankAccountNumber"></xsl:value-of>
							</BankAccountNumber>
							<AccountType>
							  <xsl:value-of select="EFTPaymentDetails/AccountType"></xsl:value-of>
							</AccountType>
							<BankName>
							  <xsl:value-of select="EFTPaymentDetails/BankName"></xsl:value-of>
							</BankName>
						  </EFTPaymentDetails>
						</xsl:when>
						<!-- This section left for Auto Pay Card. EFT Payment Details would be populated if the collection method is Auto Pay Card -->
						 <xsl:when test="CollectionMethod = 'EFTC'">
							<EFTPaymentDetails>
								<CardPaymentDetails>
									<!--<xsl:attribute name="id">
										<xsl:value-of select="@id"/>
									</xsl:attribute>-->
									<!--<xsl:copy-of select="EFTPaymentDetails/CardPaymentDetails/Token"></xsl:copy-of>-->
									<Token>
										<ProfileID>
											<xsl:value-of select="EFTPaymentDetails/CardPaymentDetails/Token/ProfileID"></xsl:value-of>
										</ProfileID>
										<PaymentProfileID>
											<xsl:value-of select="EFTPaymentDetails/CardPaymentDetails/Token/PaymentProfileID"></xsl:value-of>
										</PaymentProfileID>
									</Token>
									<CardNumber>
									  <xsl:value-of select="EFTPaymentDetails/CardPaymentDetails/CardNumberFormatted"></xsl:value-of>
									</CardNumber>								
									<ExpirationDate>
										<xsl:value-of select="EFTPaymentDetails/CardPaymentDetails/ExpirationDate"></xsl:value-of>
									</ExpirationDate>
									<CardType>
										<xsl:value-of select="EFTPaymentDetails/CardPaymentDetails/CardType"></xsl:value-of>
									</CardType>
									<CardTypeDescription>
										<xsl:value-of select="EFTPaymentDetails/CardPaymentDetails/CardTypeDescription"></xsl:value-of>
									</CardTypeDescription>
									<PaymentMethod>
										<xsl:value-of select="EFTPaymentDetails/CardPaymentDetails/PaymentMethod"></xsl:value-of>
									</PaymentMethod>
								</CardPaymentDetails>
							</EFTPaymentDetails>
						</xsl:when>
						<!-- This section left for Paper. EFT Payment Details should be NULL -->
                        <xsl:when test="CollectionMethod = 'PA'">
						</xsl:when>
						<!-- This is the replacement for the section above -->
						<xsl:otherwise>
							<xsl:copy-of select="EFTPaymentDetails"></xsl:copy-of>
						</xsl:otherwise>
					</xsl:choose>
					<ReportingGroupCode>
						<xsl:value-of select="ReportingGroupCode"></xsl:value-of>
					</ReportingGroupCode>
					<AccountStateCode>
						<xsl:value-of select="PolicyStateCode"></xsl:value-of>
					</AccountStateCode>
					<AccountReference>
						<xsl:choose>
							<xsl:when test="normalize-space(CustomAccountReference) != ''">
								<xsl:value-of select="CustomAccountReference"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="PolicyNumber"></xsl:value-of>
							</xsl:otherwise>
						</xsl:choose>
					</AccountReference>
					<AccountStatusCode>ACV</AccountStatusCode>
					<AccountTypeCode>
						<xsl:value-of select="BillType"></xsl:value-of>
					</AccountTypeCode>
					<CurrencyCulture>
						<xsl:choose>
							<!-- This section left for backward compatibility with Billing Base Product. It can be removed once we switche to the new sample product -->
							<xsl:when test="normalize-space(AccountCurrencyCulture) != ''">
								<xsl:value-of select="AccountCurrencyCulture"></xsl:value-of>
							</xsl:when>
							<xsl:otherwise>en-US</xsl:otherwise>
						</xsl:choose>
					</CurrencyCulture>
					<PolicyCurrencyCode>
						<xsl:value-of select="//session/properties/@currencyCode"/>
					</PolicyCurrencyCode>
					<AccountClassCode>
						<xsl:value-of select="BillClass"></xsl:value-of>
					</AccountClassCode>
					<HoldTypeCode>
						<xsl:value-of select="HoldTypeCode"></xsl:value-of>
					</HoldTypeCode>
					<HoldReasonCode>
						<xsl:value-of select="HoldReasonCode"></xsl:value-of>
					</HoldReasonCode>
					<HoldStartDate>
						<xsl:value-of select="HoldStartDate"></xsl:value-of>
					</HoldStartDate>
				</AccountRecord>
			</Add>
		</AccountSetup>
	</xsl:template>

	<xsl:template name="PolicyTermSetup">
		<xsl:param name="transactionTypeCode"/>
		<PolicyTermSetup>
			<LookupConfig>
				<PolicyPlanCode>
					<xsl:value-of select="PolicyPlanCode"></xsl:value-of>
				</PolicyPlanCode>
				<!-- BillClass is included here for backward compatibility. New Plan manuscripts will use Policy Plan Code. -->
				<BillClass>
					<xsl:value-of select="BillClass"></xsl:value-of>
				</BillClass>
				<PolicyLOB>
					<xsl:value-of select="PolicyLOB"></xsl:value-of>
				</PolicyLOB>
				<PolicyLOBCode>
					<xsl:value-of select="PolicyLOBCode"></xsl:value-of>
				</PolicyLOBCode>
				<PolicyTermEffectiveDate>
					<xsl:value-of select="PolicyTermEffectiveDate"/>
				</PolicyTermEffectiveDate>
				<UseMPR>
					<xsl:value-of select="UseMPR"></xsl:value-of>
				</UseMPR>
			</LookupConfig>
			<PolicyTermRecord>
				<PolicyTermEffectiveDate>
					<xsl:value-of select="PolicyTermEffectiveDate"></xsl:value-of>
				</PolicyTermEffectiveDate>
				<PolicyTermExpirationDate>
					<xsl:value-of select="PolicyTermExpirationDate"></xsl:value-of>
				</PolicyTermExpirationDate>
				<PolicyReference>
					<xsl:value-of select="PolicyNumber"></xsl:value-of>
				</PolicyReference>
				<SplitBillIndicator>N</SplitBillIndicator>
				<PolicyTermStatusCode>ACV</PolicyTermStatusCode>
				<PolicyIssueSystemCode>
					<xsl:value-of select="PolicyIssueSystemCode"/>
				</PolicyIssueSystemCode>
				<PolicyMasterCompanyCode>
					<xsl:value-of select="PolicyMasterCompanyCode"/>
				</PolicyMasterCompanyCode>
				<PolicyLineOfBusinessCode>
					<xsl:value-of select="PolicyLOBCode"/>
				</PolicyLineOfBusinessCode>
				<PolicyProductCode>
					<xsl:value-of select="PolicyProductCode"/>
				</PolicyProductCode>
				<PolicyStateCode>
					<xsl:value-of select="PolicyStateCode"></xsl:value-of>
				</PolicyStateCode>
				<ProcessingOrgUnitCode>
					<xsl:value-of select="PolicyProcessingOrgUnitCode"></xsl:value-of>
				</ProcessingOrgUnitCode>
				<TransactionTypeCode>
					<xsl:value-of select="$transactionTypeCode"/>
				</TransactionTypeCode>
			</PolicyTermRecord>
		</PolicyTermSetup>
	</xsl:template>

	<xsl:template name="DownPayment">
		<xsl:choose>
			<xsl:when test="normalize-space(//BillingDownPayment/Amount) != '' and normalize-space(//BillingDownPayment/Amount) != '0'">
				<PaymentSetup>
					<PaymentInformationRecord>
						<PaymentType>
							<xsl:value-of select="//BillingDownPayment/PaymentType"></xsl:value-of>
						</PaymentType>
						<ApplyToOption>otherAmount</ApplyToOption>
					</PaymentInformationRecord>
					<PaymentRecord>
						<EntryDate>
							<xsl:value-of select="//BillingDownPayment/EntryDate"></xsl:value-of>
						</EntryDate>
						<Amount>
							<xsl:value-of select="//BillingDownPayment/Amount"></xsl:value-of>
						</Amount>
						<CurrencyCulture>
							<xsl:value-of select="//BillingDownPayment/CurrencyCulture"></xsl:value-of>
						</CurrencyCulture>
						<PaymentMethodCode>
							<xsl:value-of select="//BillingDownPayment/PaymentMethodCode"></xsl:value-of>
						</PaymentMethodCode>
						<PostMarkDate></PostMarkDate>
						<ReportingGroupCode>1</ReportingGroupCode>
						<EntryUserId>
							<xsl:value-of select="//BillingDownPayment/EntryUserId"></xsl:value-of>
						</EntryUserId>
						<Description></Description>
						<xsl:copy-of select="//BillingDownPayment/PaymentMethodDetail"></xsl:copy-of>
					</PaymentRecord>
				</PaymentSetup>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>