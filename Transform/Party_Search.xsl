﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:template match="PartyRecord">
	<SearchRequestRecord>
	<SoundexIndicator>0</SoundexIndicator>
	<SearchCriteria>
		<PartyRecord><Locations><Location><LocationInvolvements><LocationInvolvement><ObjectTypeCode>L</ObjectTypeCode><RoleCode>OT</RoleCode></LocationInvolvement></LocationInvolvements><LocationAddressLine1 /><LocationCity /><LocationStateCode><xsl:value-of select="Location/LocationStateCode"/></LocationStateCode><LocationPostalCode /></Location></Locations><Party ><PartyFirstName></PartyFirstName><PartyMiddleName></PartyMiddleName><PartyTypeCode>P</PartyTypeCode><PartyName><xsl:value-of select="Party/PartyName"/></PartyName><PartyBirthDate /></Party><PartyEmails><PartyEmail/></PartyEmails><PartyLicenses><PartyLicense><LicenseExtendedData/><LicenseNumber /></PartyLicense></PartyLicenses><PartyPhones><PartyPhone><PhoneNumber /></PartyPhone></PartyPhones><PartyReferences><PartyReference><Reference /></PartyReference></PartyReferences><SD_PageDisplayRulesLookingForCaption>I'm looking for a:</SD_PageDisplayRulesLookingForCaption><SD_PageDisplayRulesSearchEntryCaption />
		</PartyRecord>
		<PartyNameSearchType />
	</SearchCriteria>
	<SearchControl id='SF48C55BEBAFF4DAC83E8DD926389EFB5'><Paging id='PE7A83961A7C14ED192E54CDCBB8559E4'><ListStart>1</ListStart><ListLength>100</ListLength></Paging><Filters><Filter Type='PartyType' Value='P' /><Filter Type='PartyType' Value='O' /></Filters></SearchControl><SearchRequestWorkingDatasearchType>NA</SearchRequestWorkingDatasearchType>
	</SearchRequestRecord>
	</xsl:template>
</xsl:stylesheet>

