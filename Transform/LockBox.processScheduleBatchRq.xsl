﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl" xmlns:LockBoxBatch="urn:LockBoxBatch">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="@* | node()">
      <BillingCashManagement.processScheduleBatchRq>
        <PaymentBatchScheduleRecord>
          <PaymentBatches>
            <PaymentBatch>
              <PaymentBatchId>
                <xsl:value-of select="LockBoxBatch:getBatchId()"/>
              </PaymentBatchId>
            </PaymentBatch>
          </PaymentBatches>
        </PaymentBatchScheduleRecord>
      </BillingCashManagement.processScheduleBatchRq>
    </xsl:template>
</xsl:stylesheet>
