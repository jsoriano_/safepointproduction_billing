﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/Package/1.0"
         elementFormDefault="qualified"
         xmlns="http://schemas.duckcreektech.com/Server/Package/1.0"
         xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>Requests that operate the Package distribution engine.</xs:documentation>
  </xs:annotation>
  <xs:element name="Package.deployRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>
        Gets a list of all ManuScripts and their dependent objects.
        It then gets the XML for each item in the list.
        This XML is packaged together into an install request.
        The install request is then posted to each of the specified servers.
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="servers" minOccurs="1" maxOccurs="1" type="serversType" />
        <xs:element name="items" minOccurs="1" maxOccurs="1" type="itemsType" />
      </xs:sequence>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>Status of the deployment</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="new">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="ok">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="Deployed">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="includeDependencies" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Set to 1 if you would like to include the dependencies as part of the deploy process.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.deployRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the status for each server/item.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="servers" minOccurs="1" maxOccurs="1" type="serversType" />
      </xs:sequence>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of requested failed
            syntax	- The syntax of the request was wrong or parts of the request were missing or invalid
            partial	- Only a portion of the request was able to be returned.
            warning	- The request was successful, but a message or warning was also returned
            For any value other than success, there will be an element with the same name as the attribute value
            containing a list of the details of the status.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.retrieveItemStatusDateRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Retrieves the status and the date of the item.  This is used to help determine if the item needs updated.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="items" minOccurs="1" maxOccurs="1" type="itemsType" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.retrieveItemStatusDateRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the status and the date of the item.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="items" minOccurs="1" maxOccurs="1" type="itemsType" />
      </xs:sequence>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of requested failed
            syntax	- The syntax of the request was wrong or parts of the request were missing or invalid
            partial	- Only a portion of the request was able to be returned.
            warning	- The request was successful, but a message or warning was also returned
            For any value other than success, there will be an element with the same name as the attribute value
            containing a list of the details of the status.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.changeItemsStatusRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Changes the status of the listed items on the server.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="items" minOccurs="1" maxOccurs="1" type="itemsType" />
      </xs:sequence>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>Status of the items</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="new">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="ok">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="Deployed">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.changeItemsStatusRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns only the status</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of requested failed
            syntax	- The syntax of the request was wrong or parts of the request were missing or invalid
            partial	- Only a portion of the request was able to be returned.
            warning	- The request was successful, but a message or warning was also returned
            For any value other than success, there will be an element with the same name as the attribute value
            containing a list of the details of the status.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.createInstallRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Creates an install directory used as a temporary holding place for a backup of the files to be installed.</xs:documentation>
    </xs:annotation>
    <xs:complexType></xs:complexType>
  </xs:element>
  <xs:element name="Package.createInstallRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the ID of the install directory.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of requested failed
            syntax	- The syntax of the request was wrong or parts of the request were missing or invalid
            partial	- Only a portion of the request was able to be returned.
            warning	- The request was successful, but a message or warning was also returned
            For any value other than success, there will be an element with the same name as the attribute value
            containing a list of the details of the status.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="installID" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>ID in the install directory.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.removeInstallRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Removes the install directory</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="installID" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>ID in the install directory.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.removeInstallRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns only the status.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of requested failed
            syntax	- The syntax of the request was wrong or parts of the request were missing or invalid
            partial	- Only a portion of the request was able to be returned.
            warning	- The request was successful, but a message or warning was also returned
            For any value other than success, there will be an element with the same name as the attribute value
            containing a list of the details of the status.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.backupInstallItemsRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Makes a copy of the listed items that will be rolled back to if the install should fail.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="items" minOccurs="1" maxOccurs="1" type="itemsType" />
      </xs:sequence>
      <xs:attribute name="installID" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>ID in the install directory.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.backupInstallItemsRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns only the status.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of requested failed
            syntax	- The syntax of the request was wrong or parts of the request were missing or invalid
            partial	- Only a portion of the request was able to be returned.
            warning	- The request was successful, but a message or warning was also returned
            For any value other than success, there will be an element with the same name as the attribute value
            containing a list of the details of the status.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.saveInstallItemRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Copies the install item to the server to prepare it for installation.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="item" minOccurs="1" maxOccurs="1" type="itemType" />
      </xs:sequence>
      <xs:attribute name="ID" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>ID of the resource.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="type" use="required">
        <xs:annotation>
          <xs:documentation>Type of the resource.</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="ManuScript">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="DataKey">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="DataSet">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="installID" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>ID in the install directory.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.saveInstallItemRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns only the status.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of requested failed
            syntax	- The syntax of the request was wrong or parts of the request were missing or invalid
            partial	- Only a portion of the request was able to be returned.
            warning	- The request was successful, but a message or warning was also returned
            For any value other than success, there will be an element with the same name as the attribute value
            containing a list of the details of the status.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.installItemsRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>
        Installs the items on the server associated with the installID, these are the items that have been copied over
        with the saveInstallItemRq.
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="items" minOccurs="1" maxOccurs="1" type="itemsType" />
      </xs:sequence>
      <xs:attribute name="installID" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>ID in the install directory.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>Status of the items</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="new">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="ok">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="Deployed">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Package.installItemsRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns only the status.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of requested failed
            syntax	- The syntax of the request was wrong or parts of the request were missing or invalid
            partial	- Only a portion of the request was able to be returned.
            warning	- The request was successful, but a message or warning was also returned
            For any value other than success, there will be an element with the same name as the attribute value
            containing a list of the details of the status.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation />
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="servers" type="serversType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Contains a list of servers.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:element name="items" type="itemsType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Contains a list of items.</xs:documentation>
    </xs:annotation>
  </xs:element>
  <xs:complexType name="itemsType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:element name="item" minOccurs="0" maxOccurs="unbounded" type="itemType" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="itemType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:element name="keys" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="key" minOccurs="0" maxOccurs="unbounded">
              <xs:complexType>
                <xs:attribute name="name" type="xs:string" use="required">
                  <xs:annotation>
                    <xs:documentation>Name of a key.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="value" type="xs:string" use="required">
                  <xs:annotation>
                    <xs:documentation>Value of a key.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
              </xs:complexType>
            </xs:element>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
    <xs:attribute name="ID" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>ID of the resource.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="type" use="optional">
      <xs:annotation>
        <xs:documentation>Type of the resource.</xs:documentation>
      </xs:annotation>
      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="ManuScript">
            <xs:annotation>
              <xs:documentation />
            </xs:annotation>
          </xs:enumeration>
          <xs:enumeration value="DataKey">
            <xs:annotation>
              <xs:documentation />
            </xs:annotation>
          </xs:enumeration>
          <xs:enumeration value="DataSet">
            <xs:annotation>
              <xs:documentation />
            </xs:annotation>
          </xs:enumeration>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    <xs:attribute name="description" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>Description of the resource.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="dateTime" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>date/time the resource was last updated.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="status" use="optional">
      <xs:annotation>
        <xs:documentation>Status of the items</xs:documentation>
      </xs:annotation>
      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="success">
            <xs:annotation>
              <xs:documentation />
            </xs:annotation>
          </xs:enumeration>
          <xs:enumeration value="new">
            <xs:annotation>
              <xs:documentation />
            </xs:annotation>
          </xs:enumeration>
          <xs:enumeration value="ok">
            <xs:annotation>
              <xs:documentation />
            </xs:annotation>
          </xs:enumeration>
          <xs:enumeration value="Deployed">
            <xs:annotation>
              <xs:documentation />
            </xs:annotation>
          </xs:enumeration>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="serversType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:element name="server" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="items" minOccurs="0" maxOccurs="1" type="itemsType" />
          </xs:sequence>
          <xs:attribute name="url" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>URL of the server to post to.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="userName" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>User name of the user on the server to install to.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="password" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>Password of the user on the server to install to.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="status" use="optional">
            <xs:annotation>
              <xs:documentation>Status of the items</xs:documentation>
            </xs:annotation>
            <xs:simpleType>
              <xs:restriction base="xs:string">
                <xs:enumeration value="new">
                  <xs:annotation>
                    <xs:documentation />
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="ok">
                  <xs:annotation>
                    <xs:documentation />
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="Deployed">
                  <xs:annotation>
                    <xs:documentation />
                  </xs:annotation>
                </xs:enumeration>
              </xs:restriction>
            </xs:simpleType>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
</xs:schema>