﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<xsl:template match="transaction">
		<xsl:call-template name="dc:Transaction.Body"/>
	</xsl:template>
	<xsl:template name="dc:Transaction.Body">
		<DC_Transaction Id="{@id}">
			<Type><xsl:value-of select="Type"/></Type>
			<State><xsl:value-of select="State"/></State>
			<EffectiveDate><xsl:value-of select="EffectiveDate"/></EffectiveDate>
			<ScheduleDate><xsl:value-of select="ScheduleDate"/></ScheduleDate>
			<CreatedDate><xsl:value-of select="CreatedDate"/></CreatedDate>
			<CreatedUser><xsl:value-of select="CreatedUser"/></CreatedUser>
			<OriginalCharge><xsl:value-of select="OriginalCharge"/></OriginalCharge>
			<Charge><xsl:value-of select="Charge"/></Charge>
			<ProRateFactor><xsl:value-of select="ProRateFactor"/></ProRateFactor>
			<TermPremium><xsl:value-of select="TermPremium"/></TermPremium>
			<PriorPremium><xsl:value-of select="PriorPremium"/></PriorPremium>
			<NewPremium><xsl:value-of select="NewPremium"/></NewPremium>
			<HistoryID><xsl:value-of select="HistoryID"/></HistoryID>
			<ConvertedTransactionType><xsl:value-of select="ConvertedTransactionType"/></ConvertedTransactionType>
			<CancellationDate><xsl:value-of select="CancellationDate"/></CancellationDate>
			<TransactionDate><xsl:value-of select="TransactionDate"/></TransactionDate>
			<ExpirationDate><xsl:value-of select="ExpirationDate"/></ExpirationDate>
			<Deposit><xsl:value-of select="Deposit"/></Deposit>
			<AuditCharge><xsl:value-of select="AuditCharge"/></AuditCharge>
			<AuditPremium><xsl:value-of select="AuditPremium"/></AuditPremium>
			<StatusUserContext><xsl:value-of select="StatusUserContext"/></StatusUserContext>
			<StatusUser><xsl:value-of select="StatusUser"/></StatusUser>
			<PolicyStatus><xsl:value-of select="PolicyStatus"/></PolicyStatus>
			<IssuedDate><xsl:value-of select="IssuedDate"/></IssuedDate>
			<IssuedUserName><xsl:value-of select="IssuedUserName"/></IssuedUserName>
			<ShortRateFactor><xsl:value-of select="ShortRateFactor" /></ShortRateFactor>
			<xsl:call-template name="dc:Transaction.Extensions"/>
			<xsl:call-template name="dc:Transaction.Relationships"/>
		</DC_Transaction>
	</xsl:template>
	<xsl:template name="dc:Transaction.Extensions"/>
	<xsl:template name="dc:Transaction.Relationships"/>
</xsl:stylesheet>
