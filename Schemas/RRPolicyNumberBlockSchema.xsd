﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/PolicyNumberBlock/1.0"
         elementFormDefault="qualified"
         xmlns="http://schemas.duckcreektech.com/Server/PolicyNumberBlock/1.0"
         xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>Requests to create and modify a policy's number block.</xs:documentation>
  </xs:annotation>
  <xs:element name="PolicyNumberBlock.setPolicyNumberBlockRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Sets the current block.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="blockName" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Descriptor for a block of policy numbers.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="start" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The first indicator, such as in the first policy number.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="end" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The last indicator, such as in the last policy number.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="PolicyNumberBlock.setPolicyNumberBlockRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns nothing.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
            <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
	      Describes the status of the request.
              For any value other than success, there will be an element with the same name as the attribute value containing a list of the details of the status. Also describes the status of an entity, such as a policy status of 'InForce'.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>Execution of request was successful</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>Execution of requested failed</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation>The syntax of the request was wrong or parts of the request were missing or invalid</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation>Only a portion of the request was able to be returned.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation>The request was successful, but a message or warning was also returned</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="PolicyNumberBlock.getPolicyNumberBlockRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the policy number block's information for the specified block.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="blockName" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Descriptor for a block of policy numbers.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="PolicyNumberBlock.getPolicyNumberBlockRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Return type is string.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
            <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
	      Describes the status of the request.
              For any value other than success, there will be an element with the same name as the attribute value containing a list of the details of the status. Also describes the status of an entity, such as a policy status of 'InForce'.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>Execution of request was successful</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>Execution of requested failed</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation>The syntax of the request was wrong or parts of the request were missing or invalid</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation>Only a portion of the request was able to be returned.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation>The request was successful, but a message or warning was also returned</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="start" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The first indicator, such as in the first policy number.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="end" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The last indicator, such as in the last policy number.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="current" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The current indicator, such as in the current policy number.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="PolicyNumberBlock.getPolicyNumberRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the current policy number  \for the specified block..</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="blockName" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Descriptor for a block of policy numbers.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="PolicyNumberBlock.getPolicyNumberRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns nothing.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
            <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
	      Describes the status of the request.
              For any value other than success, there will be an element with the same name as the attribute value containing a list of the details of the status. Also describes the status of an entity, such as a policy status of 'InForce'.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>Execution of request was successful</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>Execution of requested failed</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation>The syntax of the request was wrong or parts of the request were missing or invalid</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation>Only a portion of the request was able to be returned.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation>The request was successful, but a message or warning was also returned</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="policyNumber" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Identifier for a ManuScript.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="PolicyNumberBlock.deletePolicyNumberBlockRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Deletes the specified policy number block.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="blockName" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Descriptor for a block of policy numbers.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="PolicyNumberBlock.deletePolicyNumberBlockRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns nothing.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
            <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
	      Describes the status of the request.
              For any value other than success, there will be an element with the same name as the attribute value containing a list of the details of the status. Also describes the status of an entity, such as a policy status of 'InForce'.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>Execution of request was successful</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>Execution of requested failed</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation>The syntax of the request was wrong or parts of the request were missing or invalid</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation>Only a portion of the request was able to be returned.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation>The request was successful, but a message or warning was also returned</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="PolicyNumberBlock.listPolicyNumberBlocksRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns a list of policy number blocks.</xs:documentation>
    </xs:annotation>
    <xs:complexType />
  </xs:element>
  <xs:element name="PolicyNumberBlock.listPolicyNumberBlocksRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns nothing.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="policyNumberBlocks" minOccurs="1" maxOccurs="1">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="policyNumberBlock" minOccurs="0" maxOccurs="unbounded">
                <xs:complexType>
                  <xs:attribute name="status" type="xs:string" use="optional">
                    <xs:annotation>
                      <xs:documentation>
                        For any value other than success, there will be an element with the same name as the attribute value
                        containing a list of the details of the status.Also describes the status of an entity, such as a policy status of 'InForce'.
                        The status of a queueItem in the shredding Queue.
                        pending	- Waiting to be shredded.
                        failure	- Shredding of this queueItem failed.
                      </xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="start" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>The first indicator, such as in the first policy number.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="end" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>The last indicator, such as in the last policy number.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="current" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>The current indicator, such as in the current policy number.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="name" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Descriptor for an entity, such as a title.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
            <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>
	      Describes the status of the request.
              For any value other than success, there will be an element with the same name as the attribute value containing a list of the details of the status. Also describes the status of an entity, such as a policy status of 'InForce'.
          </xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>Execution of request was successful</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>Execution of requested failed</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="syntax">
              <xs:annotation>
                <xs:documentation>The syntax of the request was wrong or parts of the request were missing or invalid</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="partial">
              <xs:annotation>
                <xs:documentation>Only a portion of the request was able to be returned.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="warning">
              <xs:annotation>
                <xs:documentation>The request was successful, but a message or warning was also returned</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
</xs:schema>