﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<xsl:template match="entry">
		<xsl:call-template name="dc:TransactionReportEntry.Body"/>
	</xsl:template>
	<xsl:template name="dc:TransactionReportEntry.Body">
		<DC_TransactionReportEntry>
			<Charge><xsl:value-of select="@charge"/></Charge>
			<Count><xsl:value-of select="@count"/></Count>
			<DateTimeStamp><xsl:value-of select="@dateStamp"/></DateTimeStamp>
			<EffectiveDate><xsl:value-of select="@effectiveDate"/></EffectiveDate>
			<Index><xsl:value-of select="@index"/></Index>
			<ExampleQuoteId><xsl:value-of select="@policyID"/></ExampleQuoteId>
			<Sequence><xsl:value-of select="@sequence"/></Sequence>
			<TransactionRef><xsl:value-of select="@transGroup"/></TransactionRef>
			<TransactionType><xsl:value-of select="@transactionType"/></TransactionType>
			<Type><xsl:value-of select="@type"/></Type>
			<xsl:call-template name="dc:TransactionReportEntry.Extensions"/>
			<xsl:call-template name="dc:TransactionReportEntry.Relationships"/>
		</DC_TransactionReportEntry>
	</xsl:template>
	<xsl:template name="dc:TransactionReportEntry.Extensions"/>
	<xsl:template name="dc:TransactionReportEntry.Relationships"/>
</xsl:stylesheet>
