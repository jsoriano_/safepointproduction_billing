﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">

	<xsl:template name="dc:PremiumSubtotal.ChangeElement">
		<xsl:call-template name="dc:PremiumSubtotal.Body">
			<xsl:with-param name="Value" select="Premium"/>
			<xsl:with-param name="Change" select="change"/>
			<xsl:with-param name="Written" select="written"/>
			<xsl:with-param name="Prior" select="prior"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template match="PurePremium | PurePremiumValues | TaxesSurchargesValues | TaxesSurcharges | LineTotalPurePremium | LineTotalTaxesSurcharges | RiskTotalPurePremiums | RiskTotalTaxesSurcharges">
		<xsl:call-template name="dc:PremiumSubtotal.ChangeAttribute"/>
	</xsl:template>

	<xsl:template name="dc:PremiumSubtotal.ChangeAttribute">
		<xsl:call-template name="dc:PremiumSubtotal.Body">
			<xsl:with-param name="Value" select="text()"/>
			<xsl:with-param name="Change" select="@change[not(parent::TaxesSurcharges | parent::PurePremium)]"/>
			<xsl:with-param name="Written" select="@written"/>
			<xsl:with-param name="Prior" select="@prior"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="dc:PremiumSubtotal.PremiumChangeElement">
		<xsl:call-template name="dc:PremiumSubtotal.Body">
			<xsl:with-param name="Value" select="Premium"/>
			<xsl:with-param name="Change" select="PremiumChange"/>
			<xsl:with-param name="Written" select="PremiumWritten"/>
			<xsl:with-param name="Prior" select="PremiumPrior"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="dc:PremiumSubtotal.Body">
		<xsl:param name="Value"/>
		<xsl:param name="Change"/>
		<xsl:param name="Written"/>
		<xsl:param name="Prior"/>
		<xsl:if test="($Value != '0' or $Change != '0' or $Written != '0' or $Prior != '0')">
			<DC_PremiumSubtotal>
				<Type><xsl:value-of select="name()"/></Type>
				<Value><xsl:value-of select="$Value"/></Value>
				<Change><xsl:value-of select="$Change"/></Change>
				<Written><xsl:value-of select="$Written"/></Written>
				<Prior><xsl:value-of select="$Prior"/></Prior>
				<xsl:call-template name="dc:PremiumSubtotal.Extensions"/>
				<xsl:call-template name="dc:PremiumSubtotal.Relationships"/>
			</DC_PremiumSubtotal>
		</xsl:if>
	</xsl:template>

	<xsl:template name="dc:PremiumSubtotal.Extensions"/>
	<xsl:template name="dc:PremiumSubtotal.Relationships"/>
	
</xsl:stylesheet>
