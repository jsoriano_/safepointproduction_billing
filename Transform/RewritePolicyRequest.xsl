﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="NewPolicyTermCommon.xsl"/>
	<xsl:import href="BillingTransactionCommon.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/> 
	
	<xsl:template match="/">
		<xsl:apply-templates select="*/session/data" />
	</xsl:template>
	
	<xsl:template match="data">
		<xsl:variable name="activeTransactionId" select="(/*/_processingData/activeTransactionID)" />
		<xsl:variable name="activeTransactionTypeCode" select="(/*/_processingData/transactions/transaction[@id=$activeTransactionId]/Type)" />
		<xsl:variable name="transactionTypeCode">
			<xsl:call-template name="LookupTransactionTypeCode">
				<xsl:with-param name="code">
					<xsl:value-of select="$activeTransactionTypeCode"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<RewriteBillingProcess status="success">
			<xsl:apply-templates select="policy">
				<xsl:with-param name="previousPolicyNumber" select="policyAdmin/transactions/transaction[last()]/RewrittenFromPolicyNumber"/>
				<xsl:with-param name="transactionTypeCode" select="$transactionTypeCode"/>
			</xsl:apply-templates>
		</RewriteBillingProcess>
	</xsl:template>
</xsl:stylesheet>