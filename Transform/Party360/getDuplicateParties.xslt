﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
	<xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>

	<xsl:template match="PartyObj.listDuplicatePartiesRs/PartyRecord">
		<DuplicateParties>
			<DuplicatePartyIds >
				<xsl:variable name="varParticipantId" select="PartyRecordPartyId" />
				<xsl:for-each select="PartyMatches/PartyMatch">
					<xsl:call-template name="duplicateparties">
						<xsl:with-param name="varParticipantId" select="$varParticipantId"/>
					</xsl:call-template>
				</xsl:for-each>
			</DuplicatePartyIds>
		</DuplicateParties>
	</xsl:template>

	<!-- Duplicate Parties-->
	<xsl:template name ="duplicateparties">
		<xsl:param name="varParticipantId"/>
		<xsl:variable name="varDuplicatePartyId">
			<xsl:choose>
				<xsl:when test="PartyId = $varParticipantId">
					<xsl:value-of select="RelatedPartyId" />
				</xsl:when>
				<xsl:when test="RelatedPartyId = $varParticipantId">
					<xsl:value-of select="PartyId" />
				</xsl:when>
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$varDuplicatePartyId"/>
		<xsl:if test="position() != last() and $varDuplicatePartyId != ''">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>