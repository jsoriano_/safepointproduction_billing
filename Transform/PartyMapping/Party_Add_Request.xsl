﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="PartyRecord">
		<PartyObj.addPartyRq>
			<ReturnParty>Y</ReturnParty>
			<EntityID><xsl:value-of select="@entityId"/></EntityID>
			<TransactionDate><xsl:value-of select="@transactionDate"/></TransactionDate>
			<CalledInHierarchy>N</CalledInHierarchy>
			<PartyRecord>
				<xsl:if test="Party">
					<xsl:apply-templates select="Party"/>
				</xsl:if>
				
				<xsl:if test="Location">
					<Locations>
						<xsl:apply-templates select="Location"/>
					</Locations>
				</xsl:if>
				
				<xsl:if test="PartyPhone">
					<PartyPhones>
						<xsl:apply-templates select="PartyPhone"/>
					</PartyPhones>
				</xsl:if>

				<xsl:if test="PartyEmail">
					<PartyEmails>
						<xsl:apply-templates select="PartyEmail"/>
					</PartyEmails>
				</xsl:if>
				
				<xsl:if test="PartyInvolvement">
					<PartyInvolvements>
						<xsl:apply-templates select="PartyInvolvement"/>
					</PartyInvolvements>
				</xsl:if>
				
			</PartyRecord>
		</PartyObj.addPartyRq>
	</xsl:template>
	
	<xsl:template match="Party | Location | PartyPhone | PartyEmail | PartyInvolvement">
		<xsl:copy-of select="."/>
	</xsl:template>	

</xsl:stylesheet>
