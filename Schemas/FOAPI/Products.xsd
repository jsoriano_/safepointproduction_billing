<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
	<xs:element name="product">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="startProductActions" minOccurs="1">
					<xs:annotation>
						<xs:documentation>Identifies the starting URI invoked when the user starts a product at runtime.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element ref="pageSet" minOccurs="1" maxOccurs="unbounded">
					<xs:annotation>
						<xs:documentation>Displays one-to-many page sets based on the number of page sets defined within the product.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="effectiveDateNew" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The new business effective date for the product. If the product does not use this element, it will be set to a string, such as "Admin" or "None".</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="effectiveDateRenewal" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The renewal effective date for the product. If the product does not use this element, it will be set to a string, such as "Admin" or "None".</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="lob" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The line of business for the product.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="name" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the product.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="productId" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the stored product data. This element is a reference to the submission identifier, quote identifier, or policy identifier.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="status" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The current status of the product.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="version" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The version number for the product, such as 8.5.0.0. The first digit is versioned with each base version of the product. The second digit represents the version for the line of business. The third digit represents versioning for a circular when needed. The fourth digit represents versioning for a fix when needed.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="versionDate" use="required" type="xs:date">
				<xs:annotation>
					<xs:documentation>The version date for the product.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="pageSet">
		<xs:annotation>
			<xs:documentation>The name of a group of pages within a product.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element maxOccurs="unbounded" ref="page">
					<xs:annotation>
						<xs:documentation>Defines a page for a page set within a product.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="name" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the page set.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="startingPageSet">
				<xs:annotation>
					<xs:documentation>Defines the starting page set for a product. This element will not appear if it does not apply.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="page">
		<xs:annotation>
			<xs:documentation>Defines the fields, actions, and groups for a page.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:choice maxOccurs="unbounded">
					<xs:element ref="action">
						<xs:annotation>
							<xs:documentation>Defines all action elements available on a page.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element ref="field">
						<xs:annotation>
							<xs:documentation>Defines all fields available on a page.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element ref="group">
						<xs:annotation>
							<xs:documentation>Defines all group elements based on the number of lists on a page.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element ref="panel">
						<xs:annotation>
							<xs:documentation>Defines the fields, actions, and groups for a metapage.</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:choice>
				<xs:element minOccurs="0" ref="navigation">
					<xs:annotation>
						<xs:documentation>Defines all action elements related to page navigation.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element ref="utilityActions">
					<xs:annotation>
						<xs:documentation>Defines a group of product-level actions.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element ref="startProductActions">
					<xs:annotation>
						<xs:documentation>Identifies the starting URI invoked when the user starts a product at runtime.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="hidden" use="required">
				<xs:annotation>
					<xs:documentation>Indicates whether a page is shown or hidden.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="name" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the page.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="startingPage">
				<xs:annotation>
					<xs:documentation>Defines the starting page for a page set. This element will not appear if it does not apply.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="title" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The title of the page as it appears on a user's screen.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="panel">
		<xs:annotation>
			<xs:documentation>Defines the fields, actions, and groups for a metapage.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:choice maxOccurs="unbounded" minOccurs="0">
					<xs:element ref="action">
						<xs:annotation>
							<xs:documentation>Defines all action elements available on a metapage.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element ref="field">
						<xs:annotation>
							<xs:documentation>Defines all fields available on a metapage.</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element ref="group">
						<xs:annotation>
							<xs:documentation>Defines all group elements based on the number of lists on a metapage.</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:choice>
			</xs:sequence>
			<xs:attribute name="hidden" use="required">
				<xs:annotation>
					<xs:documentation>Indicates whether a page is shown or hidden.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="name" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the page.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="title" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>The title of the page as it appears on a user's screen.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="group">
		<xs:annotation>
			<xs:documentation>A grouping of fields and actions on a page.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:choice minOccurs="1" maxOccurs="unbounded">
				<xs:element ref="action" maxOccurs="unbounded">
					<xs:annotation>
						<xs:documentation>Defines all action elements contained within a group.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element ref="row" maxOccurs="unbounded">
					<xs:annotation>
						<xs:documentation>Groups the data row elements stored for a group of fields.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element ref="paging" maxOccurs="unbounded">
					<xs:annotation>
						<xs:documentation>Paging information for an iterative group.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:choice>
			<xs:attribute name="maximum" use="required">
				<xs:annotation>
					<xs:documentation>Indicates the maximum number of iterations for the field set on a page. This element is optional and will display a '*' when there are unlimited iterations or an integer to indicate the maximum number of iterations.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="minimum" use="required" type="xs:integer">
				<xs:annotation>
					<xs:documentation>Indicates the minimum number of iterations for the field set on a page. This element is optional.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="name" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the group.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="paging">
		<xs:annotation>
			<xs:documentation>Paging information for an iterative group.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:attribute name="page" use="required">
				<xs:annotation>
					<xs:documentation>The page to display for iterative groups </xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="perPage" use="required">
				<xs:annotation>
					<xs:documentation>The number of items in an iterative group to show on a single page.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="totalCount" use="required">
				<xs:annotation>
					<xs:documentation>The total number of iterations of the group which exist in the policy.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="row">
		<xs:annotation>
			<xs:documentation>Individual rows of field data for a data group.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:choice minOccurs="1" maxOccurs="unbounded">
				<xs:element ref="field" maxOccurs="unbounded">
					<xs:annotation>
						<xs:documentation>Defines all field elements available for a data row.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element ref="action" maxOccurs="unbounded">
					<xs:annotation>
						<xs:documentation>Defines the actions that can be applied to the data row.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element ref="group">
					<xs:annotation>
						<xs:documentation>Defines all group elements based on the number of lists on a page.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:choice>
			<xs:attribute name="id" use="required">
				<xs:annotation>
					<xs:documentation>A unique identifier for the row.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="navigation">
		<xs:annotation>
			<xs:documentation>Groups all navigation actions available for a page.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="action">
					<xs:annotation>
						<xs:documentation>Groups the actions that can be used to call a specific page.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="utilityActions">
		<xs:annotation>
			<xs:documentation>Defines all action elements which may be executed at a product-level.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="1" maxOccurs="unbounded" ref="action">
					<xs:annotation>
						<xs:documentation>Defines the actions that can be used to execute a product-level process.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="action">
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="1" maxOccurs="unbounded" ref="method">
					<xs:annotation>
						<xs:documentation>Defines the group of methods for the action in a sequential processing order. Methods can be skipped if processing is not needed.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="caption">
				<xs:annotation>
					<xs:documentation>The on-screen label assigned to the action button or link.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="contentType" use="required">
				<xs:annotation>
					<xs:documentation>Defines the type of content to be passed when invoking the URI against the API. Supported content types include XML and JSON.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="application/xml"/>
						<xs:enumeration value="application/json"/>
						<xs:enumeration value="application/multipart/form-data"/>
						<xs:enumeration value="multipart/form-data"/>
						<xs:enumeration value="text/xml"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="description" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Describes the purpose of the URI relative to the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="hidden">
				<xs:annotation>
					<xs:documentation>Indicates whether the action is shown or hidden.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="id" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:anyAttribute processContents="skip" />
		</xs:complexType>
	</xs:element>
	<xs:element name="method">
		<xs:complexType>
			<xs:sequence minOccurs="0">
				<xs:element name="body" type="xs:string" />
			</xs:sequence>
			<xs:attribute name="index" use="required" type="xs:int">
				<xs:annotation>
					<xs:documentation>Defines the index of the method to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="page" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the page associated with the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="pageSet" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the page set associated with the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="type" use="required">
				<xs:annotation>
					<xs:documentation>Defines the REST verb (e.g. GET, PUT, POST) to use when using the URI call against the API.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="GET"/>
						<xs:enumeration value="POST"/>
						<xs:enumeration value="DELETE"/>
						<xs:enumeration value="PUT"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="uri" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the name of the URI to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="field">
		<xs:complexType>
			<xs:all>
				<xs:element minOccurs="0" ref="options"/>
				<xs:element minOccurs="0" ref="action"/>
			</xs:all>
			<xs:attribute name="caption" type="xs:string">
				<xs:annotation>
					<xs:documentation>A label for a field, button, link, or displayable string for an option in a drop-down.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="controlType">
				<xs:annotation>
					<xs:documentation>Indicates the type of control for the field.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="select"/>
						<xs:enumeration value="textarea"/>
						<xs:enumeration value="text"/>
						<xs:enumeration value="radio"/>
						<xs:enumeration value="checkbox"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="dataType">
				<xs:annotation>
					<xs:documentation>Indicates the data type for the field.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="boolean"/>
						<xs:enumeration value="date"/>
						<xs:enumeration value="datetime"/>
						<xs:enumeration value="int"/>
						<xs:enumeration value="float"/>
						<xs:enumeration value="string"/>
						<xs:enumeration value="password"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="format" type="xs:string">
				<xs:annotation>
					<xs:documentation>Displays any formatting requirements specified for the data.  The format can be a displayable format for a number or displayable type, such as 'ssn', 'zipcode', or '-?,???,???,??#'.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="group" type="xs:string">
				<xs:annotation>
					<xs:documentation>Indicates the group to which the field belongs.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="hidden">
				<xs:annotation>
					<xs:documentation>Indicates whether the field is shown or hidden.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="name" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the field.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="maxLength" type="xs:integer">
				<xs:annotation>
					<xs:documentation>Indicates the maximum characters for a field.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="readOnly">
				<xs:annotation>
					<xs:documentation>Indicates whether the field is read only.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="reference" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique key for the field on the current page. This element will be a generated index at runtime and is used to find internal references keys to store the data.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="required">
				<xs:annotation>
					<xs:documentation>Indicates whether the field is required.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0|1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="value" use="required">
				<xs:annotation>
					<xs:documentation>The current value for a field, or a code value for a selection in a drop-down. If a default value is set for the field, this attribute will display that value. If no default value is set for the field, this attribute will be blank until the user supplies a value.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="options">
		<xs:annotation>
			<xs:documentation>List of selectable options for a drop-down field.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="option">
					<xs:annotation>
						<xs:documentation>Multiple option elements will exist based on the number of selectable options for the drop-down field</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="option">
		<xs:annotation>
			<xs:documentation>Groups all option elements available for a field.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:attribute name="caption" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Displays the on-screen label for the drop-down option.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="value" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Displays the system code value for the drop-down option.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="startProductActions">
		<xs:annotation>
			<xs:documentation>Identifies the starting URI invoked when the user starts a product at runtime.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="1" maxOccurs="unbounded" ref="action">
					<xs:annotation>
						<xs:documentation>Defines the actions that can be used to start a product.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>