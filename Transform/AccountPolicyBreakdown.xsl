﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<xsl:for-each select="//Account/Account">
			<Account>
				<xsl:apply-templates/>
				<PolicyTerms>
					<xsl:for-each select="//PolicyTerm">
						<PolicyTerm>
							<xsl:apply-templates/>

						</PolicyTerm>
					</xsl:for-each>
				</PolicyTerms>
			</Account>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="Account">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	
	</xsl:template>
	<xsl:template match="EFTPaymentDetails">
		<EFTPaymentDetails>
			<xsl:copy-of select="*"/>
		</EFTPaymentDetails>
	</xsl:template>
	<xsl:template match="AccountExtendedData">
		<AccountExtendedData>
			<xsl:copy-of select="*"/>
		</AccountExtendedData>
	</xsl:template>
	<xsl:template match="AccountConfig">
		<AccountConfig>
			<xsl:copy-of select="*"/>
		</AccountConfig>
	</xsl:template>
	<xsl:template match="PolicyTerm" />
	<xsl:template match="*">
		<xsl:element name="{name()}">
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="PolicyTermConfig">
		<PolicyTermConfig>
			<xsl:copy-of select="*"/>
		</PolicyTermConfig>
	</xsl:template>
	<xsl:template match="PolicyTermExtendedData">
		<PolicyTermExtendedData>
			<xsl:copy-of select="*"/>
		</PolicyTermExtendedData>
	</xsl:template>
</xsl:stylesheet>