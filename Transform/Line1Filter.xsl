﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<xsl:import href="DC_Session.xsl"/>
	<xsl:import href="DC_Policy.xsl"/>
	<xsl:import href="DC_Limit.xsl"/>
	<xsl:import href="DC_Line.xsl"/>
	<xsl:import href="DC_Coverage.xsl"/>
	<xsl:import href="DC_PremiumSubtotal.xsl"/>
	<xsl:import href="DC_TaxSurcharge.xsl"/>
	<xsl:import href="DC_Transaction.xsl"/>
	<xsl:import href="DC_TransactionReportEntry.xsl"/>
	<xsl:import href="DC_L1_Risk.xsl"/>
	<xsl:import href="DC_HelperTemplates.xsl"/>
	<xsl:include href="Carrier_Customizations.xsl"/>
	
	<xsl:template name="dc:Session.Relationships">
		<xsl:apply-templates select="data/policy"/>
		<xsl:apply-templates select="data/policyAdmin/transactions/transaction"/>
		<xsl:apply-templates select="entry"/>
	</xsl:template>
	
	<xsl:template name="dc:Policy.Relationships">
		<xsl:call-template name="dc:PremiumSubtotal.PremiumChangeElement"/>
		<xsl:apply-templates select="PurePremiumValues"/>
		<xsl:apply-templates select="TaxesSurchargesValues"/>
		<xsl:apply-templates select="LineTotalPurePremium"/>
		<xsl:apply-templates select="LineTotalTaxesSurcharges"/>
		<xsl:apply-templates select="line"/>
	</xsl:template>

	<xsl:template match="line[Type='Line1']">
		<xsl:call-template name="dc:Line.Body"/>
	</xsl:template>
	
	<xsl:template name="dc:Line.Relationships">
		<xsl:apply-templates select="coverage"/>
		<xsl:apply-templates select="risk"/>
		<xsl:apply-templates select="lineTax"/>
		<xsl:apply-templates select="limit"/>
	</xsl:template>
	
	<xsl:template name="dc:Coverage.Relationships">
		<xsl:apply-templates select="limit"/>
	</xsl:template>
	
	<xsl:template match="lineTax">
		<xsl:call-template name="dc:TaxSurcharge.Body"/>
	</xsl:template>
	
	<xsl:template name="dc:L1_Risk.Relationships">
		<xsl:apply-templates select="coverage"/>
	</xsl:template>
</xsl:stylesheet>