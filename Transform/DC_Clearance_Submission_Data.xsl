﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>	

	<xsl:template match="data">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="ACORD">
		<Party>
				<LegalEntity>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/LegalEntityCd"/>
				</LegalEntity>
				<FEIN>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='FEIN']/TaxId"/>
				</FEIN>
				<SSN>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity[TaxIdTypeCd='SSN']/TaxId"/>
				</SSN>
				<PartyName>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName"/>
				</PartyName>
				<LocationAddressLine1>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1"/>
				</LocationAddressLine1>
				<LocationAddressLine2>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2"/>
				</LocationAddressLine2>
				<LocationAddressLine3>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr3"/>
				</LocationAddressLine3>
				<LocationAddressLine4>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr4"/>
				</LocationAddressLine4>
				<LocationCity>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/City"/>
				</LocationCity>
				<LocationStateCode>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd"/>
				</LocationStateCode>
				<LocationPostalCode>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode"/>
				</LocationPostalCode>
				<LocationCounty>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Addr/County"/>
				</LocationCounty>
				<PhoneNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Phone']/PhoneNumber"/>
				</PhoneNumber>
				<FaxNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Fax']/PhoneNumber"/>
				</FaxNumber>
				<CellNumber>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Cell']/PhoneNumber"/>
				</CellNumber>
				<Email>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/EmailInfo/EmailAddr"/>
				</Email>
				<WebSite>
					<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/InsuredOrPrincipal/GeneralPartyInfo/Communications/WebsiteInfo/WebsiteURL"/>
				</WebSite>
		</Party>
		<PolicyData>
			<LOB>
				<xsl:value-of select="InsuranceSvcRq/node()[contains(name(),'QuoteInqRq')]/CommlPolicy[@id='PolicyLevel']/LOBCd"/>
			</LOB>
		</PolicyData>
	</xsl:template>

	
</xsl:stylesheet>