﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
  <xsl:template match="server/responses">
    <Policies>

	
	  <!--<Policy xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">-->
      
      <PolicyDetails xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		
        <xsl:for-each select="TransACT.getPolicySummaryRs">
          <PolicyNumber>
            <xsl:value-of select="PolicySummary/Field[@name='PolicyNumber']/@value"/>
          </PolicyNumber>
          <QuoteNumber>
            <xsl:value-of select="PolicySummary/Field[@name='QuoteNumber']/@value"/>
          </QuoteNumber>
		  <xsl:choose>
			<xsl:when test ="PolicySummary/Field[@name='PolicyStatus']/@value='Quote' or PolicySummary/Field[@name='PolicyStatus']/@value='Application'">
				<Status>Quote</Status>
			</xsl:when>
			<xsl:otherwise>
				<Status>
					<xsl:value-of select="PolicySummary/Field[@name='PolicyStatus']/@value"/>
				</Status>
			</xsl:otherwise>
		  </xsl:choose>
          <PolicyEffectiveDate>
            <xsl:value-of select="PolicySummary/Field[@name='EffectiveDate']/@value"/>
          </PolicyEffectiveDate>
          <PolicyExpirationDate>
            <xsl:value-of select="PolicySummary/Field[@name='ExpirationDate']/@value"/>
          </PolicyExpirationDate>
          <InceptionDate>
            <xsl:value-of select="PolicySummary/Field[@name='InceptionDate']/@value"/>
          </InceptionDate>
          <FullTermPremium>
            <xsl:value-of select="PolicySummary/Field[@name='FullTermPremium']/@value"/>
          </FullTermPremium>
          <PremiumWritten>
            <xsl:value-of select="PolicySummary/Field[@name='WrittenPremium']/@value"/>
          </PremiumWritten>


          <Transactions>
            <!--<xsl:call-template name="policyTransactions" />-->

            <xsl:for-each select="following-sibling::TransACT.listTransactionsRs[1]">

              <xsl:for-each select="transactions/transaction">

                <xsl:if test="(State='open' or State='committed' or State='closed') and (DeprecatedBy='' or not(DeprecatedBy) or ((Type='Report' or Type='Audit') and (DeprecatedBy!='') and (DeprecatedBy =ReplacementID)))">
                  <Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <xsl:call-template name="policyTransactionsNew" />
                  </Transaction>
                </xsl:if>
              </xsl:for-each>
            </xsl:for-each>

          </Transactions>

          <Participants>
            <xsl:call-template name="policyParticipants" />
          </Participants>

          <Activities>
            <xsl:call-template name="policyActivities" />
          </Activities>
        </xsl:for-each>

      </PolicyDetails>

      <!--</Policy>-->

    </Policies>
  </xsl:template>


  <xsl:template name="policyTransactionsNew" >
    <TypeCaption>
      <xsl:value-of select="TypeCaption"/>
    </TypeCaption>
    <Type>
      <xsl:value-of select="Type"/>
    </Type>
    <EffectiveDate>
      <xsl:value-of select="EffectiveDate"/>
    </EffectiveDate>
    <NewPremium>
      <xsl:value-of select="NewPremium"/>
    </NewPremium>
    <Charge>
      <xsl:value-of select="Charge"/>
    </Charge>
	<Status>
      <xsl:value-of select="Status"/>
    </Status>
  </xsl:template>


  <xsl:template name="policyParticipants" >
    <xsl:for-each select="../PartyInvolvement.getParticipantsRs/Participants/Participant">
    <Participant>
      <!--<PartyId>
        <xsl:value-of select="ParticipantId"/>
      </PartyId>-->
      <!--<xsl:call-template name="partydetailRoles" />-->
      <xsl:variable name="varParticipantId" select="ParticipantId" />
      <xsl:for-each select="../../../PartyObj.getPartyLiteRs/PartyRecord">
        <xsl:if test ="PartyRecordPartyId=$varParticipantId" >
          <PartyId>
            <xsl:value-of select="PartyRecordPartyId"/>
          </PartyId>
          <PartyFullName>
            <xsl:value-of select="Party/PartyCorrespondenceName"/>
          </PartyFullName>
          <PartyName>
            <xsl:value-of select="Party/PartyName"/>
          </PartyName>
          <PartyFirstName>
            <xsl:value-of select="Party/PartyFirstName"/>
          </PartyFirstName>
          <PartyMiddleName>
            <xsl:value-of select="Party/PartyMiddleName"/>
          </PartyMiddleName>
          <PartyNamePrefix>
            <xsl:value-of select="Party/PartyNamePrefix"/>
          </PartyNamePrefix>
          <PartyNameSuffix>
            <xsl:value-of select="Party/PartyNameSuffix"/>
          </PartyNameSuffix>
          <PartyTypeCode>
            <xsl:value-of select="Party/PartyTypeCode"/>
          </PartyTypeCode>
          <xsl:call-template name="partydetailRoles" />
        </xsl:if>
      </xsl:for-each>
      </Participant>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="partydetailRoles" >
    <RoleCodes>
      <xsl:variable name="varParticipantId" select="PartyRecordPartyId" />
      <xsl:for-each select="../../PartyInvolvement.getParticipantsRs/Participants/Participant">
        <xsl:if test ="ParticipantId=$varParticipantId" >
          <!--<PartyId>
            <xsl:value-of select="ParticipantId"></xsl:value-of>
          </PartyId>-->
          <xsl:for-each select="ObjectTypeCodes/ObjectTypeCode/RoleCodes">
            <xsl:for-each select="RoleCode">
	      <xsl:sort select="." data-type="text" order="ascending"/>
              <Role>
                <RoleCode>
                  <xsl:value-of select="current()"/>
                </RoleCode>
              </Role>
            </xsl:for-each>
          </xsl:for-each>

        </xsl:if>
      </xsl:for-each>

    </RoleCodes>
  </xsl:template>
  
  <xsl:template name="policyActivities" >    
    <xsl:copy-of select="../OnlineData.getActivitiesRs/Activities/Paging/TotalCount"/>    
    <xsl:for-each select="../OnlineData.getActivitiesRs/Activities/Activity">
      <Activity>
        <xsl:copy-of select="ActivityId"/>
        <xsl:copy-of select="EntityId"/>
        <Description>
          <xsl:value-of select="ActivityDescription"/>
        </Description>
        <Performer>
          <xsl:value-of select="UserFullName"/>
        </Performer>        
        <xsl:copy-of select="ActivityType"/>
        <Date>
          <xsl:value-of select="ActivityDateTime"/>
        </Date>
      </Activity>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
