﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/BatchPolicy/1.0"
         elementFormDefault="qualified"
         xmlns="http://schemas.duckcreektech.com/Server/BatchPolicy/1.0"
         xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>Defines requests which allow batch modification of policies.</xs:documentation>
  </xs:annotation>
  <xs:element name="BatchPolicy.listRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Lists policies based on a script.  This gives the user a way to preview a script.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="script" type="scriptType" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.listRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Lists policies based on a script.  This gives the user a way to preview a script.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="policyList" minOccurs="0" maxOccurs="unbounded">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="policy" minOccurs="0" maxOccurs="unbounded">
                <xs:complexType>
                  <xs:attribute name="policyID" type="xs:IDREF" use="required">
                    <xs:annotation>
                      <xs:documentation>ID of the policy.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="ClientName" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Name of the client.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="clientID" type="xs:IDREF" use="required">
                    <xs:annotation>
                      <xs:documentation>Id of the client.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="status" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Describes the status of the policy</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="LOB" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Line of business</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="policyNumber" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Number of the policy.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="description" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Description of the policy.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="effectiveDate" type="xs:date" use="required">
                    <xs:annotation>
                      <xs:documentation>Date the policy is effective.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="expirationDate" type="xs:date" use="required">
                    <xs:annotation>
                      <xs:documentation>Date the policy expires.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="modifiedDate" type="xs:date" use="required">
                    <xs:annotation>
                      <xs:documentation>Date the policy was last modified.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="accessedDate" type="xs:date" use="required">
                    <xs:annotation>
                      <xs:documentation>Date the policy was last accessed.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute name="startIndex" type="xs:int" use="required">
              <xs:annotation>
                <xs:documentation>The start index of the log item to retrieve within the specified query.</xs:documentation>
              </xs:annotation>
            </xs:attribute>
            <xs:attribute name="listCount" type="xs:int" use="required">
              <xs:annotation>
                <xs:documentation>Number of policies returned.</xs:documentation>
              </xs:annotation>
            </xs:attribute>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.updateRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Updates policies based on a script.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="script" type="scriptType" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.updateRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Updates policies based on a script.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="batchLog" type="batchLogType" />
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.listLogRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Retrieves a list of the policies that have been modified based on a script.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="keys" type="keysType" minOccurs="0" />
      </xs:sequence>
      <xs:attribute name="startIndex" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>The start index of the log item to retrieve within the specified query.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="maxItems" type="xs:int" use="optional">
        <xs:annotation>
          <xs:documentation>The maximum number of log items to retrieve within the specified query.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.listLogRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Retrieves a list of the policies that have been modified based on a script.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="batchLog" type="batchLogType" />
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.listSummaryRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Retrieves a summary list of the items that have been modified by a script.</xs:documentation>
    </xs:annotation>
    <xs:complexType />
  </xs:element>
  <xs:element name="BatchPolicy.listSummaryRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See BatchPolicy.listSummaryRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="listSummary" minOccurs="0" maxOccurs="unbounded">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="job" minOccurs="0" maxOccurs="unbounded">
                <xs:complexType>
                  <xs:attribute name="logName" type="xs:string" use="required">
                    <xs:annotation>
                      <xs:documentation>Name of a batch job.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="pk" type="xs:IDREF" use="required">
                    <xs:annotation>
                      <xs:documentation>Primary key.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="successCount" type="xs:int" use="optional">
                    <xs:annotation>
                      <xs:documentation>Number of items that processed successfully by the batch process.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="totalCount" type="xs:int" use="required">
                    <xs:annotation>
                      <xs:documentation>Total number of items that were processed by the batch process.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="failureCount" type="xs:int" use="optional">
                    <xs:annotation>
                      <xs:documentation>Number of items that failed to process.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.deleteLogByNameRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Deletes all the entries of a Batch Job given the name of the job.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="jobName" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Name of the batch job.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.deleteLogByNameRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See BatchPolicy.deleteLogByNameRq.</xs:documentation>
    </xs:annotation>
    <!--xs:annotation>
						<xs:documentation>
							
							See BatchPolicy.deleteLogItemRq.
						</xs:documentation>
					</xs:annotation-->
    <xs:complexType>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.deleteLogItemRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Delete an individual log item.  An entry is created for each policy/message processed.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="logItemID" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>ID of a log item in the Batch Log tables.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.deleteLogItemRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:complexType>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.listLogKeysRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>The keys that can be queried for the ListLog call.</xs:documentation>
    </xs:annotation>
    <xs:complexType />
  </xs:element>
  <xs:element name="BatchPolicy.listLogKeysRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>The keys that can be queried for the ListLog call.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="response" minOccurs="0" maxOccurs="unbounded">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="keys" type="keysType" />
              <xs:element name="operators" minOccurs="0" maxOccurs="unbounded">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="operator" minOccurs="0" maxOccurs="unbounded">
                      <xs:complexType>
                        <xs:attribute name="name" use="required">
                          <xs:annotation>
                            <xs:documentation>An operator symbol.</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:enumeration value="&lt;">
                                <xs:annotation>
                                  <xs:documentation>Less than</xs:documentation>
                                </xs:annotation>
                              </xs:enumeration>
                              <xs:enumeration value="&gt;">
                                <xs:annotation>
                                  <xs:documentation>Greater than</xs:documentation>
                                </xs:annotation>
                              </xs:enumeration>
                              <xs:enumeration value="=">
                                <xs:annotation>
                                  <xs:documentation>Equal to</xs:documentation>
                                </xs:annotation>
                              </xs:enumeration>
                              <xs:enumeration value="!=">
                                <xs:annotation>
                                  <xs:documentation>Not equal to</xs:documentation>
                                </xs:annotation>
                              </xs:enumeration>
                              <xs:enumeration value="&lt;=">
                                <xs:annotation>
                                  <xs:documentation>Less than or equal to</xs:documentation>
                                </xs:annotation>
                              </xs:enumeration>
                              <xs:enumeration value="&gt;=">
                                <xs:annotation>
                                  <xs:documentation>Greater than or equal to</xs:documentation>
                                </xs:annotation>
                              </xs:enumeration>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name="connectors" minOccurs="0" maxOccurs="unbounded">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="connector" minOccurs="0" maxOccurs="unbounded">
                      <xs:complexType>
                        <xs:attribute name="name" use="required">
                          <xs:annotation>
                            <xs:documentation>A logical connector that creates a group of statements from two subgroups.</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction>
                              <xs:enumeration value="AND">
                                <xs:annotation>
                                  <xs:documentation>The group is true if both subgroups evaluate to true.</xs:documentation>
                                </xs:annotation>
                              </xs:enumeration>
                              <xs:enumeration value="OR">
                                <xs:annotation>
                                  <xs:documentation>The group is true if either or both subgroups evaluate to true.</xs:documentation>
                                </xs:annotation>
                              </xs:enumeration>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.deleteLogRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Delete a batch job using the ID of the batch.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="logID" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>ID of a batch job.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.deleteLogRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See BatchPolicy.deleteLogRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.deleteRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Deletes policies based on a script.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="script" type="scriptType" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="BatchPolicy.deleteRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Deletes policies based on a script.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="batchLog" type="batchLogType" />
      </xs:sequence>
      <xs:attribute name="status" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>
            Describes the status of the response:
            success	- Execution of request was successful
            failure	- Execution of request failed
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="scriptType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:element name="name" type="xs:string" />
      <xs:element name="processType" type="xs:string" />
      <xs:element name="scriptType" type="xs:string" />
      <xs:element name="emailFrom" type="xs:string" />
      <xs:element name="emailTo" type="xs:string" />
      <xs:element name="log" type="xs:string" />
      <xs:element name="logType" type="xs:string" />
      <xs:element name="duplicatePolicy" type="xs:string" minOccurs="0" />
      <xs:element name="deletePermanently" type="xs:string" minOccurs="0" />
      <xs:element name="createTransaction" type="xs:string" />
      <xs:element name="skipIfInLog" type="xs:string" />
      <xs:element name="store" type="xs:string" />
      <xs:element name="shredNow" type="xs:string" />
      <xs:element name="shredDataNow" type="xs:string" />
      <xs:element name="query" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="keys" type="keysType" minOccurs="0" />
            <xs:element name="sql" type="xs:string" minOccurs="0" msdata:Ordinal="1" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" />
          </xs:sequence>
          <xs:attribute name="advanced" use="required">
            <xs:annotation>
              <xs:documentation>Indicates whether the query should be built from name/value pairs, or whether an SQL query is provided.</xs:documentation>
            </xs:annotation>
            <xs:simpleType>
              <xs:restriction base="xs:boolean">
                <xs:enumeration value="0">
                  <xs:annotation>
                    <xs:documentation>Name/value pairs determine the query.  Default</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="1">
                  <xs:annotation>
                    <xs:documentation>An SQL query is defined in the script.</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
              </xs:restriction>
            </xs:simpleType>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
      <xs:element name="includeInLog" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:attribute name="manuScript" type="xs:IDREF" use="required">
            <xs:annotation>
              <xs:documentation>ID of a manuScript.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="manuScriptCaption" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>Preserved only for the BatchScriptEditor application.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="field" type="xs:IDREF" use="required">
            <xs:annotation>
              <xs:documentation>ID of a field in a ManuScript.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
      <xs:element name="fields" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="item" minOccurs="0" maxOccurs="unbounded">
              <xs:complexType>
                <xs:attribute name="id" type="xs:IDREF" use="optional">
                  <xs:annotation>
                    <xs:documentation>Used by the BatchPolicyApp when editing items in a spreadsheet this is ignored during execution of a script.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="Field" type="xs:IDREF" use="required">
                  <xs:annotation>
                    <xs:documentation>The name of the field to executed within the ManuScript.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="ManuScript" type="xs:IDREF" use="required">
                  <xs:annotation>
                    <xs:documentation>The ManuScript to execute for each policy during a script if empty then the ManuScript associated with the policy will be executed.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="ManuScriptCaption" type="xs:string" use="required">
                  <xs:annotation>
                    <xs:documentation>Preserved only for the BatchScriptEditor application.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="ValueFrom" use="required">
                  <xs:annotation>
                    <xs:documentation>If Data then the value of the field is retrieved from the existing policy, if Calculated then the ManuScript/Field are executed to retrieve the value.</xs:documentation>
                  </xs:annotation>
                  <xs:simpleType>
                    <xs:restriction base="xs:string">
                      <xs:enumeration value="Data">
                        <xs:annotation>
                          <xs:documentation />
                        </xs:annotation>
                      </xs:enumeration>
                      <xs:enumeration value="Calculated">
                        <xs:annotation>
                          <xs:documentation />
                        </xs:annotation>
                      </xs:enumeration>
                    </xs:restriction>
                  </xs:simpleType>
                </xs:attribute>
              </xs:complexType>
            </xs:element>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="batchLogType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <!--xs:element name="script" minOccurs="1" maxOccurs="" type="scriptType"/-->
      <xs:element name="script" type="scriptType" />
      <xs:element name="batchLogItem" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>
            <xs:element name="batchLogNameValue" minOccurs="0" maxOccurs="unbounded">
              <xs:complexType>
                <xs:attribute name="name" type="xs:string" use="required">
                  <xs:annotation>
                    <xs:documentation>The name of a name/value pair.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute name="value" type="xs:string" use="required">
                  <xs:annotation>
                    <xs:documentation>The value of a name/value pair.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
              </xs:complexType>
            </xs:element>
          </xs:sequence>
          <xs:attribute name="ClientName" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>Name of the client.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="LOB" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>Line of business</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="QuoteID" type="xs:IDREF" use="optional">
            <xs:annotation>
              <xs:documentation>ID of the quote.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="itemID" type="xs:IDREF" use="optional">
            <xs:annotation>
              <xs:documentation>ID of the item</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="Status" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>Policy status.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="batchStatus" use="required">
            <xs:annotation>
              <xs:documentation>The status of an individual policy acted upon by a script.</xs:documentation>
            </xs:annotation>
            <xs:simpleType>
              <xs:restriction base="xs:string">
                <xs:enumeration value="success">
                  <xs:annotation>
                    <xs:documentation>Policy actions succeeded</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="failure">
                  <xs:annotation>
                    <xs:documentation>Policy actions failed</xs:documentation>
                  </xs:annotation>
                </xs:enumeration>
              </xs:restriction>
            </xs:simpleType>
          </xs:attribute>
          <xs:attribute name="QuoteStatus" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>Status of a quote.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="EffectiveDate" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>Effective Date of a policy.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="startTime" type="xs:dateTime" use="optional">
            <xs:annotation>
              <xs:documentation>The time the script was started.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="endTime" type="xs:dateTime" use="optional">
            <xs:annotation>
              <xs:documentation>The time the script completed.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="itemError" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>Error description for a batch log.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="pk" type="xs:IDREF" use="optional">
            <xs:annotation>
              <xs:documentation>Primary key.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
    <xs:attribute name="startTime" type="xs:dateTime" use="optional">
      <xs:annotation>
        <xs:documentation>The time the script was started.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="endTime" type="xs:dateTime" use="optional">
      <xs:annotation>
        <xs:documentation>The time the script completed.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="logName" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>Name of a batch job.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="count" type="xs:int" use="optional">
      <xs:annotation>
        <xs:documentation>The number of items in the query, but not necessarily returned.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="startIndex" type="xs:int" use="optional">
      <xs:annotation>
        <xs:documentation>The start index of the log item to retrieve within the specified query.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="logError" type="xs:string" use="optional">
      <xs:annotation>
        <xs:documentation>Error description for a batch log.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="itemType" use="optional">
      <xs:annotation>
        <xs:documentation>The itmes that were processed by the batch: policies or messages.</xs:documentation>
      </xs:annotation>
      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="Policies">
            <xs:annotation>
              <xs:documentation />
            </xs:annotation>
          </xs:enumeration>
          <xs:enumeration value="Messages">
            <xs:annotation>
              <xs:documentation />
            </xs:annotation>
          </xs:enumeration>
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
  </xs:complexType>
  <xs:complexType name="keysType" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence>
      <xs:element name="key" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:attribute name="id" type="xs:IDREF" use="optional">
            <xs:annotation>
              <xs:documentation>Used by the BatchPolicyApp when editing items in a spreadsheet this is ignored during execution of a script.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="name" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>The name of a name/value pair.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="op" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>SQL operators</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="value" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>The value of a name/value pair.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="connect" use="optional">
            <xs:annotation>
              <xs:documentation>SQL compound where statement connectors.</xs:documentation>
            </xs:annotation>
            <xs:simpleType>
              <xs:restriction base="xs:string">
                <xs:enumeration value="AND">
                  <xs:annotation>
                    <xs:documentation />
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="OR">
                  <xs:annotation>
                    <xs:documentation />
                  </xs:annotation>
                </xs:enumeration>
              </xs:restriction>
            </xs:simpleType>
          </xs:attribute>
          <xs:attribute name="type" type="xs:string" use="optional">
            <xs:annotation>
              <xs:documentation>Key type.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
      <xs:element name="orderKey" minOccurs="0" maxOccurs="unbounded">
        <xs:complexType>
          <xs:attribute name="name" type="xs:string" use="required">
            <xs:annotation>
              <xs:documentation>The name of a name/value pair.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          <xs:attribute name="direction" use="required">
            <xs:annotation>
              <xs:documentation>SQL order by sort direction.</xs:documentation>
            </xs:annotation>
            <xs:simpleType>
              <xs:restriction base="xs:string">
                <xs:enumeration value="asc">
                  <xs:annotation>
                    <xs:documentation />
                  </xs:annotation>
                </xs:enumeration>
                <xs:enumeration value="desc">
                  <xs:annotation>
                    <xs:documentation />
                  </xs:annotation>
                </xs:enumeration>
              </xs:restriction>
            </xs:simpleType>
          </xs:attribute>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
</xs:schema>